package com.viet.le.hangmaneurekaclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
public class HangmanEurekaClientController {
    @Autowired
    private DiscoveryClient discoveryClient;
    @Autowired
    private RestTemplate restTemplate;


    @RequestMapping("/")
    public String home() {
        return "Hello world";
    }

    @RequestMapping("/greeting")
    public String greeting() {
        return "Hello from EurekaClient!";
    }

    /**
     * call 'hangman-registration-eureka-client' service through eureka
     * reference: https://howtodoinjava.com/spring-cloud/spring-cloud-service-discovery-netflix-eureka/
     * @param name
     * @return
     */
    @RequestMapping(value = "/greeting/{name}", method = RequestMethod.GET)
    public String getRegisrationInfo(@PathVariable String name) {
        ResponseEntity<String> responseEntity = this.restTemplate
                        .exchange("http://hangman-registration-eureka-client/greeting/" + name,
                HttpMethod.GET, null, String.class);
        String response = responseEntity.getBody();
        return response;
    }

//    @RequestMapping("/service-instances/{applicationName}")
//    public List<ServiceInstance> serviceInstancesByApplicationName(
//            @PathVariable String applicationName) {
//        return this.discoveryClient.getInstances(applicationName);
//    }
}
